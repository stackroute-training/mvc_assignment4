﻿using System.ComponentModel.DataAnnotations;
namespace mvc_validation.Models
{
    public class Employee
    {
        [Required(ErrorMessage ="Please Enter a Valid Response")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter a Valid Response")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter a Valid Response")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter a Valid Response")]
        public string Role { get; set; }
        [Required(ErrorMessage = "Please Enter a Valid Response")]
        public string Location  { get; set; }
    }
}
