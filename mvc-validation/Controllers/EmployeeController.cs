﻿using Microsoft.AspNetCore.Mvc;
using mvc_validation.Models;

namespace mvc_validation.Controllers
{
    public class EmployeeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                return Json(employee);
            }
            else
            {
                return View();
            }
        }
    }
}
